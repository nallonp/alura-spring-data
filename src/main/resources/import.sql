INSERT INTO spring_data.cargos(descricao) VALUES ('Analista de RH');
INSERT INTO spring_data.cargos(descricao) VALUES ('Engenheiro de software');
INSERT INTO spring_data.cargos(descricao) VALUES ('Gerente');

INSERT INTO spring_data.funcionarios(salario, cpf, data_de_contratacao, nome, cargo_id) VALUES (3000.00, '123', '2021-04-10', 'Maria da Silva', 1);
INSERT INTO spring_data.funcionarios(salario, cpf, data_de_contratacao, nome, cargo_id) VALUES (4000.00, '123', '2021-04-11', 'Pedro de Souza', 2);
INSERT INTO spring_data.funcionarios(salario, cpf, data_de_contratacao, nome, cargo_id) VALUES (5000.00, '123', '2021-04-12', 'Jose Oliveira', 3);
INSERT INTO spring_data.funcionarios(salario, cpf, data_de_contratacao, nome, cargo_id) VALUES (4000.00, '123', '2021-04-13', 'Maria Oliveira', 1);
INSERT INTO spring_data.funcionarios(salario, cpf, data_de_contratacao, nome, cargo_id) VALUES (5000.00, '123', '2021-04-14', 'Maria Mendonça', 2);
INSERT INTO spring_data.funcionarios(salario, cpf, data_de_contratacao, nome, cargo_id) VALUES (6000.00, '123', '2021-04-15', 'Maria Jordão', 1);
INSERT INTO spring_data.funcionarios(salario, cpf, data_de_contratacao, nome, cargo_id) VALUES (7000.00, '123', '2021-04-16', 'Maria Meireles', 1);

INSERT INTO spring_data.unidades_de_trabalho(descricao, endereco) VALUES ('São Paulo', 'Rua São Paulo');
INSERT INTO spring_data.unidades_de_trabalho(descricao, endereco) VALUES ('Rio de Janeiro', 'Rua Rio de Janeiro');
INSERT INTO spring_data.unidades_de_trabalho(descricao, endereco) VALUES ('Minas Gerais', 'Rua Minas Gerais');

INSERT INTO spring_data.funcionarios_unidades(funcionario_id, unidade_de_trabalho_id) VALUES (1,1);
INSERT INTO spring_data.funcionarios_unidades(funcionario_id, unidade_de_trabalho_id) VALUES (1,2);
INSERT INTO spring_data.funcionarios_unidades(funcionario_id, unidade_de_trabalho_id) VALUES (1,3);
INSERT INTO spring_data.funcionarios_unidades(funcionario_id, unidade_de_trabalho_id) VALUES (2,2);
INSERT INTO spring_data.funcionarios_unidades(funcionario_id, unidade_de_trabalho_id) VALUES (3,3);
INSERT INTO spring_data.funcionarios_unidades(funcionario_id, unidade_de_trabalho_id) VALUES (4,3);
INSERT INTO spring_data.funcionarios_unidades(funcionario_id, unidade_de_trabalho_id) VALUES (5,3);
INSERT INTO spring_data.funcionarios_unidades(funcionario_id, unidade_de_trabalho_id) VALUES (6,3);
INSERT INTO spring_data.funcionarios_unidades(funcionario_id, unidade_de_trabalho_id) VALUES (7,3);
