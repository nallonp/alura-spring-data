package br.com.alura.spring.data.service;

import br.com.alura.spring.data.orm.Cargo;
import br.com.alura.spring.data.orm.Funcionario;
import br.com.alura.spring.data.orm.UnidadeTrabalho;
import br.com.alura.spring.data.repository.CargoRepository;
import br.com.alura.spring.data.repository.FuncionarioRepository;
import br.com.alura.spring.data.repository.UnidadeTrabalhoRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Service
public class CrudFuncionarioService {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private final FuncionarioRepository funcionarioRepository;
    private final CargoRepository cargoRepository;
    private final UnidadeTrabalhoRepository unidadeTrabalhoRepository;
    private Boolean system = true;

    public CrudFuncionarioService(FuncionarioRepository funcionarioRepository,
                                  CargoRepository cargoRepository,
                                  UnidadeTrabalhoRepository unidadeTrabalhoRepository) {
        this.funcionarioRepository = funcionarioRepository;
        this.cargoRepository = cargoRepository;
        this.unidadeTrabalhoRepository = unidadeTrabalhoRepository;
    }

    public void inicial(Scanner scanner) {
        while (system) {
            System.out.println("Qual ação de funcionário deseja executar?");
            System.out.println("0 - Sair");
            System.out.println("1 - Salvar");
            System.out.println("2 - Atualizar");
            System.out.println("3 - Visualizar");
            System.out.println("4 - Deletar");
            System.out.println("9 - Buscar um funcionário.");
            int action = scanner.nextInt();
            switch (action) {
                case 1 -> salvar(scanner);
                case 2 -> atualizar(scanner);
                case 3 -> visualizar(scanner);
                case 4 -> deletar(scanner);
                case 9 -> visualizarUm(scanner);
                default -> system = false;
            }
        }
    }

    private void salvar(Scanner scanner) {
        Funcionario funcionario = new Funcionario();
        System.out.println("Nome do funcionário: ");
        funcionario.setNome(scanner.next());
        System.out.println("CPF do funcionário:");
        funcionario.setCpf(scanner.next());
        System.out.println("Salário do funcionário:");
        funcionario.setSalario(new BigDecimal(scanner.next()));
        System.out.println("Qual a data de contratação?dd/MM/yyyy");
        funcionario.setDataDeContratacao(LocalDate.parse(scanner.next(), formatter));
        System.out.println("Qual o id do cargo?");
        Optional<Cargo> optionalCargo = cargoRepository.findById(scanner.nextLong());
        optionalCargo.ifPresent(funcionario::setCargo);
        List<UnidadeTrabalho> unidadesTrabalho = unidade(scanner);
        funcionario.setUnidadesDeTrabalho(unidadesTrabalho);
        funcionarioRepository.save(funcionario);
        System.out.println("Salvo!");
    }

    private void atualizar(Scanner scanner) {
        Funcionario funcionario = new Funcionario();
        System.out.println("ID do funcionário:");
        funcionario.setId(scanner.nextLong());
        System.out.println("Nome do funcionário: ");
        funcionario.setNome(scanner.next());
        System.out.println("CPF do funcionário:");
        funcionario.setCpf(scanner.next());
        System.out.println("Salário do funcionário:");
        funcionario.setSalario(new BigDecimal(scanner.next()));
        System.out.println("Qual a data de contratação?dd/MM/yyyy");
        funcionario.setDataDeContratacao(LocalDate.parse(scanner.next(), formatter));
        System.out.println("Qual o id do cargo?");
        Optional<Cargo> optionalCargo = cargoRepository.findById(scanner.nextLong());
        optionalCargo.ifPresent(funcionario::setCargo);
        List<UnidadeTrabalho> unidadesTrabalho = unidade(scanner);
        funcionario.setUnidadesDeTrabalho(unidadesTrabalho);
        funcionarioRepository.save(funcionario);
        System.out.println("Atualizado!");
    }

    private void visualizar(Scanner scanner) {
        System.out.println("Que página você quer visualizar? ");
        Integer page = scanner.nextInt();
        PageRequest pageable = PageRequest.of(page, 5, Sort.by(Sort.Direction.ASC, "nome"));
        Page<Funcionario> funcionarios = funcionarioRepository.findAll(pageable);
        System.out.println(funcionarios);
        System.out.println("Página atual: " + funcionarios.getNumber());
        System.out.println("Número total de elementos: " + funcionarios.getTotalElements());
        funcionarios.forEach(System.out::println);
    }

    private void deletar(Scanner scanner) {
        System.out.println("Id do funcionário:");
        Long id = scanner.nextLong();
        funcionarioRepository.deleteById(id);
        System.out.println("Deletado...");
    }

    private void visualizarUm(Scanner scanner) {
        System.out.println("Digite o ID do funcionário que deseja visualizar: ");
        Optional<Funcionario> funcionarioOptional = funcionarioRepository.findById(scanner.nextLong());
        funcionarioOptional.ifPresentOrElse(System.out::println, () -> System.out.println("Funcionario não encontrado."));
    }

    private List<UnidadeTrabalho> unidade(Scanner scanner) {
        boolean isTrue = true;
        List<UnidadeTrabalho> unidades = new ArrayList<>();

        while (isTrue) {
            System.out.println("Digite o unidadeId (Para sair digite 0)");
            long unidadeId = scanner.nextLong();
            if (unidadeId != 0) {
                Optional<UnidadeTrabalho> unidade = unidadeTrabalhoRepository.findById(unidadeId);
                unidade.ifPresent(value -> unidades.add(unidade.get()));
            } else isTrue = false;
        }
        return unidades;
    }

}
