package br.com.alura.spring.data.service;

import br.com.alura.spring.data.orm.UnidadeTrabalho;
import br.com.alura.spring.data.repository.UnidadeTrabalhoRepository;
import org.springframework.stereotype.Service;

import java.util.Scanner;

@Service
public class CrudUnidadeTrabalhoService {
    private final UnidadeTrabalhoRepository unidadeTrabalhoRepository;
    private Boolean system = true;

    public CrudUnidadeTrabalhoService(UnidadeTrabalhoRepository unidadeTrabalhoRepository) {
        this.unidadeTrabalhoRepository = unidadeTrabalhoRepository;
    }

    public void inicial(Scanner scanner) {
        while (system) {
            System.out.println("Qual ação de cargo deseja executar?");
            System.out.println("0 - Sair");
            System.out.println("1 - Salvar");
            System.out.println("2 - Atualizar");
            System.out.println("3 - Visualizar");
            System.out.println("4 - Deletar");
            int action = scanner.nextInt();

            switch (action) {
                case 1 -> salvar(scanner);
                case 2 -> atualizar(scanner);
                case 3 -> visualizar();
                case 4 -> deletar(scanner);
                default -> system = false;
            }
        }
    }

    private void salvar(Scanner scanner) {
        UnidadeTrabalho unidadeTrabalho = new UnidadeTrabalho();
        System.out.println("Descrição da unidade de trabalho: ");
        String descricao = scanner.next();
        unidadeTrabalho.setDescricao(descricao);
        System.out.println("Endereço da unidade de trabalho: ");
        unidadeTrabalho.setEndereco(scanner.next());
        unidadeTrabalhoRepository.save(unidadeTrabalho);
        System.out.println("Salvo!");
    }

    private void atualizar(Scanner scanner) {
        UnidadeTrabalho unidadeTrabalho = new UnidadeTrabalho();
        System.out.println("ID da unidade de trabalho que gostaria de atualizar: ");
        unidadeTrabalho.setId(scanner.nextLong());
        System.out.println("Descrição da unidade de trabalho: ");
        unidadeTrabalho.setDescricao(scanner.next());
        System.out.println("Endereço da unidade de trabalho: ");
        unidadeTrabalho.setEndereco(scanner.next());
        unidadeTrabalhoRepository.save(unidadeTrabalho);
        System.out.println("Salvo.");
    }

    private void visualizar() {
        Iterable<UnidadeTrabalho> unidadesDeTrabalho = unidadeTrabalhoRepository.findAll();
        unidadesDeTrabalho.forEach(System.out::println);
    }

    private void deletar(Scanner scanner) {
        System.out.println("ID da unidade de trabalho que gostaria de deletar: ");
        unidadeTrabalhoRepository.deleteById(scanner.nextLong());
        System.out.println("Deletado...");
    }
}
