package br.com.alura.spring.data.repository;

import br.com.alura.spring.data.orm.Funcionario;
import br.com.alura.spring.data.orm.FuncionarioProjecao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface FuncionarioRepository extends PagingAndSortingRepository<Funcionario, Long>, JpaSpecificationExecutor<Funcionario> {

    /*@Override
    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH,
            attributePaths = {"unidadesDeTrabalho",
                    "cargo"})
    @EntityGraph(value = "funcionarios-entity-graph")
    Iterable<Funcionario> findAll();*/
    List<Funcionario> findByNome(String nome);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"unidadesDeTrabalho", "cargo"})
    List<Funcionario> findTop2ByNomeLike(String nome);

    @Query("SELECT f FROM Funcionario f WHERE f.nome LIKE :nome AND f.salario >= :salario AND f.dataDeContratacao = :data")
    List<Funcionario> findNomeSalarioMaiorDataContratacao(String nome, Double salario, LocalDate data);

    /*@Query(value = "SELECT f.id, f.nome, f.cpf, f.salario, f.data_de_contratacao, f.cargo_id, c.id, c.descricao, ut.id, ut.descricao, ut.endereco, fu.unidade_de_trabalho_id, fu.funcionario_id " +
            "FROM spring_data.funcionarios f " +
            "INNER JOIN spring_data.cargos c ON f.cargo_id = c.id " +
            "INNER JOIN spring_data.funcionarios_unidades fu ON f.id = fu.funcionario_id " +
            "INNER JOIN spring_data.unidades_de_trabalho ut ON fu.unidade_de_trabalho_id = ut.id " +
            "WHERE f.data_de_contratacao >= :data ",
            nativeQuery = true)*/
    @Query("SELECT f FROM Funcionario f " +
            "JOIN FETCH f.cargo " +
            "JOIN FETCH f.unidadesDeTrabalho " +
            "WHERE f.dataDeContratacao >= :data "
    )
    List<Funcionario> findDataContratacaoMaior(LocalDate data);

    @Override
        //@EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"unidadesDeTrabalho", "cargo"})
    Page<Funcionario> findAll(Pageable pageable);

    @Query(value = "SELECT f.id, f.nome, f.salario FROM spring_data.funcionarios f",
            nativeQuery = true)
    List<FuncionarioProjecao> findFuncionarioSalario();
}
