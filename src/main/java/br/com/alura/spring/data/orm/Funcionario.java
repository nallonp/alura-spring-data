package br.com.alura.spring.data.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "funcionarios")
public class Funcionario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;
    private String cpf;
    private BigDecimal salario;
    @Column(nullable = false)
    private LocalDate dataDeContratacao;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, foreignKey = @ForeignKey(name = "cargo_id_fkey"))
    private Cargo cargo;
    @ManyToMany
    @JoinTable(name = "funcionarios_unidades",
            joinColumns = {@JoinColumn(name = "funcionario_id", foreignKey = @ForeignKey(name = "funcionario_id_fkey"))},
            inverseJoinColumns = {@JoinColumn(name = "unidade_de_trabalho_id", foreignKey = @ForeignKey(name = "unidade_de_trabalho_id_fkey"))},
            uniqueConstraints = @UniqueConstraint(name = "func_id_uni_trab_id_uq", columnNames = {"funcionario_id", "unidade_de_trabalho_id"}))
    private List<UnidadeTrabalho> unidadesDeTrabalho;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public BigDecimal getSalario() {
        return salario;
    }

    public void setSalario(BigDecimal salario) {
        this.salario = salario;
    }

    public LocalDate getDataDeContratacao() {
        return dataDeContratacao;
    }

    public void setDataDeContratacao(LocalDate dataDeContratacao) {
        this.dataDeContratacao = dataDeContratacao;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public List<UnidadeTrabalho> getUnidadesDeTrabalho() {
        return unidadesDeTrabalho;
    }

    public void setUnidadesDeTrabalho(List<UnidadeTrabalho> unidadesDeTrabalho) {
        this.unidadesDeTrabalho = unidadesDeTrabalho;
    }

    @Override
    public String toString() {
        return "Funcionario{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                ", salario=" + salario +
                ", dataDeContratacao=" + dataDeContratacao +
                '}';
    }
}
