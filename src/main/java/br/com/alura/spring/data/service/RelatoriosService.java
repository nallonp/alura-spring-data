package br.com.alura.spring.data.service;

import br.com.alura.spring.data.orm.Funcionario;
import br.com.alura.spring.data.orm.FuncionarioProjecao;
import br.com.alura.spring.data.repository.FuncionarioRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

@Service
public class RelatoriosService {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private final FuncionarioRepository funcionarioRepository;
    private Boolean system = true;

    public RelatoriosService(FuncionarioRepository funcionarioRepository) {
        this.funcionarioRepository = funcionarioRepository;
    }

    public void inicial(Scanner scanner) {
        while (system) {
            System.out.println("Qual ação de cargo deseja executar?");
            System.out.println("0 - Sair");
            System.out.println("1 - Buscar um funcionário por nome: ");
            System.out.println("2 - Buscar funcionário com parâmetros nome, data, salario: ");
            System.out.println("3 - Buscar funcionário com data de contratação maior/igual a: ");
            System.out.println("4 - Relatório de funcionário/salário");
            System.out.println("9 - Top 2 por nome: ");
            int action = scanner.nextInt();
            switch (action) {
                case 1 -> buscaFuncionarioNome(scanner);
                case 2 -> buscaFuncionarioNomeSalarioMaiorData(scanner);
                case 3 -> buscaFuncionarioPorDataDeContratacao(scanner);
                case 4 -> pesquisaFuncionarioSalario();
                case 9 -> ListarTop2PorNome(scanner);
                default -> system = false;
            }
        }
    }

    private void buscaFuncionarioNome(Scanner scanner) {
        System.out.println("Qual nome deseja pesquisar: ");
        List<Funcionario> funcionarios = funcionarioRepository.findByNome(scanner.next());
        funcionarios.forEach(System.out::println);

    }

    private void buscaFuncionarioNomeSalarioMaiorData(Scanner scanner) {
        System.out.println("Qual nome deseja pesquisar?");
        String nome = scanner.next();

        System.out.println("Qual salário deseja pesquisar?");
        Double salario = scanner.nextDouble();

        System.out.println("Qual data deseja pesquisar?");
        LocalDate data = LocalDate.parse(scanner.next(), formatter);

        List<Funcionario> list = funcionarioRepository.findNomeSalarioMaiorDataContratacao(nome, salario, data);
        list.forEach(System.out::println);
    }

    private void buscaFuncionarioPorDataDeContratacao(Scanner scanner) {
        System.out.println("Qual data de contratação deseja pesquisar?");
        LocalDate data = LocalDate.parse(scanner.next(), formatter);
        List<Funcionario> list = funcionarioRepository.findDataContratacaoMaior(data);
        list.forEach(System.out::println);
    }

    private void ListarTop2PorNome(Scanner scanner) {
        System.out.println("Digite o nome que quer buscar: ");
        List<Funcionario> funcionarios = funcionarioRepository.findTop2ByNomeLike("%" + scanner.next() + "%");
        if (funcionarios.isEmpty()) {
            System.out.println("Nenhum funcionário com o nome especificado foi encontrado.");
            return;
        }
        System.out.println("Funcionários encontrados: ");
        funcionarios.forEach(System.out::println);
    }

    private void pesquisaFuncionarioSalario() {
        List<FuncionarioProjecao> list = funcionarioRepository.findFuncionarioSalario();
        System.out.println("Lista de funcionários: ");
        System.out.println("ID:\tNome:\t\t\tSalário:");
        list.forEach(f -> System.out.println(f.getId() + "\t" + f.getNome() + "\t" + f.getSalario()));
    }
}
